SET @b=0;

select * from (SELECT
    t_b.user_id,
    t_b.ballance - @b AS ballance_delta,
    @b := t_b.ballance Ballance,
    t_b.time
    FROM ballance t_b	
) as wrapper
where 1 = 1 
and ballance_delta != 0
and time > DATE(NOW()) - INTERVAL 8 DAY
ORDER BY time
