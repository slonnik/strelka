# drop user if exists
grant usage on *.* to 'slonnik'@'%';
drop user 'slonnik'@'%';

# create user 'slonnik' again
create user slonnik identified by 'Deda19770802';

# grant permissions to user 'slonnik'
grant all privileges on *.* to 'slonnik'@'%';

# create schema 'mediator' for 'mediator' service
drop database if exists mediator;
create database mediator;

# create user 'mediator' for 'mediator' schema
grant usage on *.* to 'mediator'@'localhost';
drop user 'mediator'@'localhost';
create user 'mediator'@'localhost' identified by  'Deda19770802';
grant all privileges on mediator.* to 'mediator'@'localhost' ;

# switch to 'mediator' schema
use mediator;

# create 'ballance' table
create table ballance( 
	id int not null auto_increment,
	card_id varchar(255) not null,
	ballance varchar(255) not null,
	time text not null,
	primary key(id),
	index idx_card_id (card_id(128)));

