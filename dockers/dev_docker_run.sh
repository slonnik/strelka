#!/bin/bash
# run dev container
CONTAINER_NAME='strelka_dev'
BOT_TOCKEN='395071837:AAEKHGBaEPv3_sYRcJyeiy_yCaQcWTKLqOE'
IMAGE='slonnik/strelka:dev'
CMD='bash'
PORT=8443
ENTRY_POINT="--entrypoint='/root/start.sh'"

if [ ! "$(docker ps -q -f name=$CONTAINER_NAME)" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=$CONTAINER_NAME)" ]; then
        echo "removing container $CONTAINER_NAME"
        docker rm $CONTAINER_NAME
    fi
    echo "creating container $CONTAINER_NAME"		
    docker run --name=$CONTAINER_NAME  -it  -w '/root/strelka' -e BOT_TOCKEN=$BOT_TOCKEN -e PORT=$PORT -p $PORT:$PORT $ENTRY_POINT  $IMAGE  $CMD
fi

