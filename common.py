import json
import urllib2
import pika
import threading
import datetime
import os
import redis
from pymongo import MongoClient

# connection to rabbitMQ
global connection
connection = pika.BlockingConnection(
	pika.ConnectionParameters(host=os.environ['AMQ_HOST'],port=int(os.environ['AMQ_PORT']), 
				credentials = pika.credentials.PlainCredentials('guest','guest')))

# class to store available queue names
class Queue:
        # command queue
        EXCHANGE_CMD = 'EXCHANGE_CMD'

# class to store available event codes
class Event:
        # get ballance event
        EVENT_GET_BALLANCE = 'EVENT_GET_BALLANCE'
	# return up to date ballance event
        EVENT_BALLANCE = 'EVENT_BALLANCE'
	# return up to date ballance by timer
	EVENT_TIMER_BALLANCE = 'EVENT_TIMER_BALLANCE'
	# return current ballance from history service
	EVENT_HISTORY_CURRENT_BALLANCE = 'EVENT_HISTORY_CURRENT_BALLANCE'
        # event history ballance from history service
	EVENT_HISTORY_BALLANCE = 'EVENT_HISTORY_BALLANCE' 
	# event  timer ballance from history servcie
	EVENT_HISTORY_TIMER_BALLANCE  = 'EVENT_HISTORY_TIMER_BALLANCE'
	# start monitoing event
        EVENT_GO = 'EVENT_GO'
        # stop monitoring event
        EVENT_SLEEP = 'EVENT_SLEEP'
        # register user event
        EVENT_REGISTER_USER = 'EVENT_REGISTER_USER'
	# heartbit event
        EVENT_HB = 'EVENT_HB'
	# proxy new message
        EVENT_NEW_PROXY_MESSAGE = 'EVENT_NEW_PROXY_MESSAGE'
	# timer event
	EVENT_TIMER = 'EVENT_TIMER'
	#get history
	EVENT_GET_HISTORY = 'EVENT_GET_HISTORY'


# class to store message keys
class MessageKey:
        SOURCE = 'source'
        EVENT = 'event'
        BODY = 'body'


USERS_HOST = os.environ['USERS_HOST']
USERS_PORT = os.environ['USERS_PORT']
PROXY_HOST = os.environ['PROXY_HOST']
PROXY_PORT = os.environ['PROXY_PORT']


# MQ consumer class
class MQConsumer(threading.Thread):

        def __init__(self, exchange, callback, daemon = True):
                threading.Thread.__init__(self)
                self.exchange = exchange
                self.callback = callback
                self.daemon = daemon

        def run(self):
                channel = connection.channel()
                channel.exchange_declare(exchange=self.exchange,
                                type='fanout')
                result = channel.queue_declare(exclusive=True)
                queue_name = result.method.queue
                channel.queue_bind(exchange=self.exchange,
                                queue=queue_name)
                channel.basic_consume(self.callback,
                        queue=queue_name,
                        no_ack=True)
                channel.start_consuming()

# class to publish MQ messages
class MQPublisher:

        @staticmethod
        def publish(exchange, data):

                channel = connection.channel()
                channel.exchange_declare(exchange=exchange,
                                 type='fanout')
                channel.basic_publish(exchange=exchange,
                                routing_key='',
                                body=json.dumps(data))
		channel.close()

# class to publish command messages
class CmdMQPublisher:

        @staticmethod
        def publish(data):

                channel = connection.channel()
                channel.exchange_declare(exchange = Queue.EXCHANGE_CMD,
                                 type='fanout')
                channel.basic_publish(exchange = Queue.EXCHANGE_CMD,
                                routing_key='',
                                body=json.dumps(data))


# Command consumer class
class CmdMQConsumer(threading.Thread):

        def __init__(self, callback, daemon = True):
                threading.Thread.__init__(self)
                self.callback = callback
                self.daemon = daemon

        def run(self):
                channel = connection.channel()
                channel.exchange_declare(exchange=Queue.EXCHANGE_CMD,
                                type='fanout')
                result = channel.queue_declare(exclusive=True)
                queue_name = result.method.queue
                channel.queue_bind(exchange=Queue.EXCHANGE_CMD,
                                queue=queue_name)
                channel.basic_consume(self.callback,
                        queue=queue_name,
                        no_ack=True)
                channel.start_consuming()

# base event handler class
class EventHandler:
        # ctor
        def __init__(self, event):
                self.event = event
        # check if we can handle this event
        def can_handle(self, event):
                return self.event ==  event

        # handle event
        def handle(self, data):
                pass

# class to process events
class EventProcessor():
        event_handlers = []
	@staticmethod
        def process(data):
		event = data.get(MessageKey.EVENT)
                for handler in EventProcessor.event_handlers :
                        if handler.can_handle(event) :
                                # we can handle this request
                                handler.handle(data.get(MessageKey.BODY))
	
	@staticmethod
        def callback(ch, method, properties, body):
		data = json.loads(body)
        	EventProcessor.process(data)



# base HB handler
class HBEventHandler(EventHandler):
	def __init__(self, logger):
		EventHandler.__init__(self, Event.EVENT_HB)
		self.logger = logger	

	def handle(self, data):
		self.logger.info(data)

# proxy to communicate with Users service
class UserClient:
	# get user by user id
	@staticmethod
	def get_user_by_id(user_id):
                req = urllib2.Request('http://{}:{}/users/{}'.format(USERS_HOST, USERS_PORT, user_id))
                response = urllib2.urlopen(req)
      		return json.loads(response.read())

	# get user by card id
	@staticmethod
	def get_user_by_card_id(card_id):
		req = urllib2.Request('http://{}:{}/cards/{}'.format(USERS_HOST, USERS_PORT, card_id))
                response = urllib2.urlopen(req)
		return json.loads(response.read())
	
	# get all users, subscribed for "uptime updaes"
	@staticmethod
	def get_subscribed_users():
		req = urllib2.Request('http://{}:{}/users/subscribed'.format(USERS_HOST, USERS_PORT))
                response = urllib2.urlopen(req)
                return json.loads(response.read())

# get mongodb client
def get_mongo_db(dbname):
	return MongoClient("mongodb://{}:{}".format(os.environ['MONGO_HOST'], os.environ['MONGO_PORT']))[dbname]	

def get_redis_client(dbname):
	return  redis.StrictRedis(host=os.environ['REDIS_HOST'], port=os.environ['REDIS_PORT'], db=dbname)
