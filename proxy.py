#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import telegram
from flask import Flask, request
import threading
import json
import ssl
import logging
import logging.handlers
import common
import locale
import datetime
from common import MessageKey
from common import Queue
from common import Event
import os
import locale


app = Flask(__name__)

#telegram bot
global bot

#conversation cache
global redis

bot = telegram.Bot(token=os.environ['BOT_TOCKEN'])
logging.basicConfig(filename='/var/log/proxy.log',level=logging.ERROR)

loggly = logging.getLogger('loggly')
loggly.setLevel(logging.ERROR)

#add handler to the logger
handler = logging.handlers.SysLogHandler('/dev/log')

#add formatter to the handler
formatter = logging.Formatter('Proxy: {"message":"%(message)s"}')

handler.formatter = formatter
loggly.addHandler(handler)


def get_local_time(time):
	locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')
	return datetime.datetime.strptime(time, "%Y-%m-%d %H:%M:%S").strftime("%a, %d %b %Y %H:%M")

# class to store all configuration values
class Configuration:
        SOURCE = 'PROXY'



@app.route('/HOOK', methods=['POST'])
def webhook_handler():
        update = telegram.Update.de_json(request.get_json(force=True), bot)
	if update.message is None:
		loggly.warn('empty message arrived.')
		return 'ok'

	user = update.message.from_user
	tags = update.message.text.split(' ')
	command = tags[0]
	redis.set(user.id, update.message.chat.id)
        # command to register user and card
	if command == '/card':
		try:
        		card_id = tags[1]
        		body = {'info' : {
                		'nick' : user.username,
                        	'name' : user.first_name
                         	},
                        	'id' : user.id,
                        	'card_id' : card_id,
                        	'dynamic_update' : '0'
				}
		
               		data = {MessageKey.SOURCE : Configuration.SOURCE,
                       		MessageKey.EVENT : Event.EVENT_REGISTER_USER,
                        	MessageKey.BODY : body}
                	common.CmdMQPublisher.publish(data = data)
		except Exception as ex:
			loggly.error(str(ex))
	# command to start polling ballance
	elif command == '/go':
		data = {MessageKey.SOURCE : Configuration.SOURCE,
                        MessageKey.EVENT : Event.EVENT_GO,
                        MessageKey.BODY : {'id' : user.id}}
               	common.CmdMQPublisher.publish(data = data)
	# command to stop ballance polling
	elif command == '/sleep':
               	data = {MessageKey.SOURCE : Configuration.SOURCE,
                       	MessageKey.EVENT : Event.EVENT_SLEEP,
                       	MessageKey.BODY : {'id' : user.id}}
             	common.CmdMQPublisher.publish(data = data)
	# command to get hisory
        elif command == '/history':
		user = common.UserClient.get_user_by_id(user_id = user.id)
                data = {MessageKey.SOURCE : Configuration.SOURCE,
                        MessageKey.EVENT : Event.EVENT_GET_HISTORY,
                        MessageKey.BODY : {'card_id' : user.get('card_id'), 'msg_id' : update.message.message_id}}
                common.CmdMQPublisher.publish(data = data)
        else:
  		user = common.UserClient.get_user_by_id(user_id = user.id)
          	data = {MessageKey.SOURCE : Configuration.SOURCE,
                      	MessageKey.EVENT : Event.EVENT_GET_BALLANCE,
                      	MessageKey.BODY : {'card_id' : user.get('card_id'), 'msg_id' : update.message.message_id}}
        	common.CmdMQPublisher.publish(data = data)

        return 'ok'


@app.route('/set_webhook', methods=['GET', 'POST'])
def set_webhook():
    s = bot.setWebhook("https://slonnik.ru:{}/HOOK".format(common.PROXY_PORT))
    if s:
        return "webhook setup ok"
    else:
        return "webhook setup failed"


@app.route('/')
def index():
    with open('/home/slonnik/projects/strelka/index.html') as file:  
	return file.read() 


#class to consume Hitory ballance
class HistoryBallanceHandler(common.EventHandler):
	def __init__(self):
		common.EventHandler.__init__(self, Event.EVENT_HISTORY_BALLANCE)
        def handle(self, data):
		card_id = data['card_id']	
		user = common.UserClient.get_user_by_card_id(card_id)
		user_id = user.get('id')
		chat_id = redis.get(user_id)
		delta = int(data['delta'])
		time = get_local_time(time = data['time'])
		if delta < 0:
			bot.send_message(chat_id = chat_id, text = "Оплата: {} [{}]".format(float(-delta)/100, time))	
		elif delta > 0:
			bot.send_message(chat_id = chat_id, text = "Пополнение: {} [{}]".format(float(delta)/100, time))
		bot.send_message(chat_id = chat_id, text = "<b>{}</b>".format(float(data['text'])/100), parse_mode = telegram.ParseMode.HTML)

#class to consrumeA timer history ballance
class TimerBallanceHandler(common.EventHandler):
	def __init__(self):
                common.EventHandler.__init__(self, Event.EVENT_HISTORY_TIMER_BALLANCE)
        def handle(self, data):
                card_id = data['card_id']
                user = common.UserClient.get_user_by_card_id(card_id)
                user_id = user.get('id')
                chat_id = redis.get(user_id)
                delta = int(data['delta'])
		time = get_local_time(time = data['time'])
                if delta < 0:
                        bot.send_message(chat_id = chat_id, text = "Оплата: {} [{}]".format(float(-delta)/100, time))
                elif delta > 0:
                        bot.send_message(chat_id = chat_id, text = "Пополнение: {} [{}]".format(float(delta)/100, time))
                bot.send_message(chat_id = chat_id, text = "<b>{}</b>".format(float(data['text'])/100), parse_mode = telegram.ParseMode.HTML)

#class to consrume current  history ballance
class CurrentBallanceHandler(common.EventHandler):
        def __init__(self):
                common.EventHandler.__init__(self, Event.EVENT_HISTORY_CURRENT_BALLANCE)
        def handle(self, data):
                card_id = data['card_id']
                user = common.UserClient.get_user_by_card_id(card_id)
                user_id = user.get('id')
                chat_id = redis.get(user_id)
                bot.send_message(chat_id = chat_id, text = "<b>{}</b>".format(float(data['text'])/100), parse_mode = telegram.ParseMode.HTML)


if __name__ == "__main__":
	#define events we want to listen
	common.EventProcessor.event_handlers = [CurrentBallanceHandler(), TimerBallanceHandler(), HistoryBallanceHandler()]
	# start listening for  events
        common.CmdMQConsumer(callback = common.EventProcessor.callback).start()
	#init cache
	redis = common.get_redis_client(0)
    	# finally start server      
    	ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    	ctx.load_cert_chain('/root/strelka/cert/strelka.pem', '/root/strelka/cert/strelka.key')
    	app.run(host=common.PROXY_HOST, port=common.PROXY_PORT, ssl_context=ctx)
