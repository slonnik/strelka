#!/usr/bin/python2.7
import pika
import threading
import json
import json
import time
import redis
import logging
import logging.handlers
import datetime
import common
from common import MessageKey
from common import Queue
from common import Event

logging.basicConfig(filename='/var/log/timer.log',level=logging.ERROR)

loggly = logging.getLogger('loggly')
loggly.setLevel(logging.ERROR)

#add handler to the logger
handler = logging.handlers.SysLogHandler('/dev/log')

#add formatter to the handler
formatter = logging.Formatter('Timer: {"message":"%(message)s"}')

handler.formatter = formatter
loggly.addHandler(handler)

#cinfiguration class
class Configuration:
	# source name
	SOURCE = 'TIMER'

class Work(threading.Thread):
        def __init__(self, timeout, event, source = Configuration.SOURCE):
                threading.Thread.__init__(self)
		self.timeout = timeout
		self.source = source
		self.event = event

        def run(self):
		while True:
			data = {MessageKey.SOURCE : self.source, MessageKey.EVENT: self.event}
			common.CmdMQPublisher.publish(data = data)
			time.sleep(self.timeout)	

if __name__ == "__main__":
	works = [Work(timeout = 300, event = Event.EVENT_HB) ,Work(timeout = 120, event = Event.EVENT_TIMER)]				
	for work in works:
		work.start()
