#!/usr/bin/python2.7
from flask import Flask, request
import logging
import logging.handlers
import pymongo
import common
from common import Event
from common import MessageKey
import datetime
import pytz
from pytz import timezone

logging.basicConfig(filename='/var/log/history.log',level=logging.ERROR)
loggly = logging.getLogger('loggly')
loggly.setLevel(logging.ERROR)

#add handler to the logger
handler = logging.handlers.SysLogHandler('/dev/log')

# reference to history database
global db

class Configuration:
	SOURCE = 'history'


#add formatter to the handler
formatter = logging.Formatter('History: {"message":"%(message)s"}')

handler.formatter = formatter
loggly.addHandler(handler)


# get ballance by card id
def get_ballance_by_card_id(card_id):
	events = db.history.find({'body.card_id' : card_id}).sort("time", pymongo.DESCENDING).limit(1)
	for event in  events[:]:
		loggly.info(event)
		return str(event.get('body').get('text'))
	return '0'

#update ballance for particular user
def update_ballance(data, event):
	# use Moscow time zone
	msk_timezone = timezone('Europe/Moscow')
	msk_now = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC).astimezone(msk_timezone)
	now = msk_now.strftime("%Y-%m-%d %H:%M:%S")
	db.history.insert_one({'event' : event, 'time' : now, 'body' : data})
	return now

#class to consume ballance
class BallanceHandler(common.EventHandler):
	def __init__(self):
		common.EventHandler.__init__(self, Event.EVENT_BALLANCE)
	
        def handle(self, data):
		try:
			card_id = data['card_id']
			history_value = get_ballance_by_card_id(card_id)
			delta = int(data['text']) - int(history_value)
			data['delta'] = delta	
			if delta != 0:
                                # update history
                                data['time'] = update_ballance(data=data, event=Event.EVENT_BALLANCE)
			loggly.error('Message ballance: {}'.format(data))	
			#publish history ballance to the queue
                        out_data = {MessageKey.SOURCE : Configuration.SOURCE,
                                                MessageKey.EVENT : Event.EVENT_HISTORY_CURRENT_BALLANCE,
                                                MessageKey.BODY : data}
                        common.CmdMQPublisher.publish(data = out_data)
					
		except Exception as ex:
			loggly.error(str(ex))


#class to consume timer ballance
class TimerBallanceHandler(common.EventHandler):
        def __init__(self):
                common.EventHandler.__init__(self, Event.EVENT_TIMER_BALLANCE)

        def handle(self, data):
                try:
                        card_id = data['card_id']
                        history_value = get_ballance_by_card_id(card_id)
                        delta = int(data['text']) - int(history_value)
                        data['delta'] = delta
                        if delta != 0:
                                # update history
                                data['time'] = update_ballance(data=data, event=Event.EVENT_TIMER_BALLANCE)
                                #publish history ballance to the queue
                                out_data = {MessageKey.SOURCE : Configuration.SOURCE,
                                                MessageKey.EVENT : Event.EVENT_HISTORY_TIMER_BALLANCE,
                                                MessageKey.BODY : data}
                                common.CmdMQPublisher.publish(data = out_data)
                except Exception as ex:
                        loggly.error(str(ex))


# class  to handle history request
class HistoryHandler(common.EventHandler):
	def __init__(self):
		common.EventHandler.__init__(self, Event.EVENT_GET_HISTORY)

	def handle(self, data):
		try:
			history_date = (datetime.datetime.now()-datetime.timedelta(days=7)).strftime("%Y-%m-%d %H:%M:%S")
			card_id = data['card_id'] 
			query = {'time': {'$gt' : '{}'.format(history_date)}}
			events = db.history.find({'time' : {'$gt' : '{}'.format(history_date)}})
			for event in events[:]:
				data = {'card_id' : card_id}
				data['text'] = event.get('body').get('text')
				data['delta'] = event.get('body').get('delta')
				data['time'] = event.get('time')
				out_data = {MessageKey.SOURCE : Configuration.SOURCE,
                                                MessageKey.EVENT : Event.EVENT_HISTORY_BALLANCE,
                                                MessageKey.BODY : data}
                        	common.CmdMQPublisher.publish(data = out_data)
		except Exception as ex:
			loggly.error(str(ex))
			

if __name__ == "__main__":
	# connect to history storage
        db = common.get_mongo_db(dbname='history')
	# define events we are going to listen
	common.EventProcessor.event_handlers = [BallanceHandler(), HistoryHandler(), TimerBallanceHandler(),  common.HBEventHandler(loggly)]
	# start listening for ballance update
        common.CmdMQConsumer(common.EventProcessor.callback, daemon = False).start()
