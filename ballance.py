#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
import pika
import threading
import urllib2
import json
import logging
import logging.handlers
import common
from common import MessageKey
from common import Event

logging.basicConfig(filename='/var/log/ballance.log',level=logging.ERROR)
loggly = logging.getLogger('loggly')
loggly.setLevel(logging.ERROR)

#add handler to the logger
handler = logging.handlers.SysLogHandler('/dev/log')

#add formatter to the handler
formatter = logging.Formatter('Ballance: {"message":"%(message)s"}')

handler.formatter = formatter
loggly.addHandler(handler)

# users card to be update dynamically
global cards_to_update
cards_to_update = set([])

# class to store all configuration values
class Configuration:
	# source name
	SOURCE = 'BALLANCE'
	
# get ballance from remote host
def get_ballance(card_id):
	url = 'http://strelkacard.ru/api/cards/status/?cardnum={}&cardtypeid=3ae427a1-0f17-4524-acb1-a3f50090a8f3'.format(card_id)
        req = urllib2.Request(url)
        opener = urllib2.build_opener()
        f = opener.open(req)
        data  = json.loads(f.read())
	if 'balance' in data:
		return data['balance'] #we have good result
	# something goes wrong
	raise Exception()	


# class to handle 'EVENT_GET_BALLANCE' event
class GetBallanceHandler(common.EventHandler):
	def __init__(self):
		common.EventHandler.__init__(self, Event.EVENT_GET_BALLANCE)
	
	def handle(self, data):
		loggly.info(data)
		try:
			# first get client card
			card_id = data['card_id']			
			# query ballance
			data['text'] = get_ballance(card_id)
			loggly.error('GetBallance : {}'.format(data))	
			# publish ballance to the queue
			out_data = {MessageKey.SOURCE : Configuration.SOURCE, 
					MessageKey.EVENT : Event.EVENT_BALLANCE,
                                	MessageKey.BODY : data}
                        common.CmdMQPublisher.publish(data = out_data)
		
		except Exception as ex:
			loggly.error(str(ex))
		

# class to consume timer
class TimerHandler(common.EventHandler):
        def __init__(self):
                common.EventHandler.__init__(self, Event.EVENT_TIMER)

        def handle(self, data):
		# get ballance from history
		for card_id in cards_to_update:
			try:
				 # get the latest ballance
                        	ballance = get_ballance(card_id)
                        	body = {'card_id' : card_id, 'text' : ballance}
                        	out_data = {MessageKey.SOURCE : Configuration.SOURCE,
                                	MessageKey.EVENT : Event.EVENT_TIMER_BALLANCE,
                                	MessageKey.BODY : body}
                        	common.CmdMQPublisher.publish(data = out_data)	
			except Exception as ex:
				loggly.error(str(ex))

		
# class to handle 'EVENT_GO' event
class GoEventHandler(common.EventHandler):
        def __init__(self):
                common.EventHandler.__init__(self, Event.EVENT_GO)

        def handle(self, data):
		try:
			card_id = data.get('card_id')
			if card_id not in cards_to_update:
				cards_to_update.add(card)
		except Exception as ex:
			loggly.error(str(ex))

# class to handle 'EVENT_SLEEP' event
class SleepEventHandler(common.EventHandler):
        def __init__(self):
                common.EventHandler.__init__(self, Event.EVENT_SLEEP)

        def handle(self, data):
		try:
			card_id = data.get('card_id')
			if card_id in cards_to_update:
				cards_to_update.remove(card_id)
		except Exception as ex:
			loggly.error(str(ex))

if __name__ == "__main__":
	# query card to be dynamically updated	
	try:
		for client in  common.UserClient.get_subscribed_users():
			if int(client.get('dynamic_update')) == 1:
				cards_to_update.add(client.get('card_id'))
	except Exception as ex:
		loggly.error(str(ex))
	# define what events we are going to listen
	common.EventProcessor.event_handlers = [GetBallanceHandler(), TimerHandler(), GoEventHandler(),
						SleepEventHandler(), common.HBEventHandler(loggly)]
	# start listening for 'EVENT_CMD' queue
        common.CmdMQConsumer(callback = common.EventProcessor.callback, daemon = False).start()

