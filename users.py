#!/usr/bin/python2.7
from flask import Flask, request
#import json
from bson.json_util import dumps
import logging
import logging.handlers
import common
from common import Queue
from common import Event

app = Flask(__name__)

logging.basicConfig(filename='/var/log/users.log',level=logging.ERROR)
loggly = logging.getLogger('loggly')
loggly.setLevel(logging.ERROR)

#add handler to the logger
handler = logging.handlers.SysLogHandler('/dev/log')

# users database
global db

#add formatter to the handler
formatter = logging.Formatter('Users: {"message":"%(message)s"}')

handler.formatter = formatter
loggly.addHandler(handler)

class Configuration:
	SOURCE = 'HISTORY'

# get all users or users by id
@app.route('/users/<int:user_id>', methods=['GET'])
def get_user_by_id(user_id):
	# get user by id
	user = db.users.find_one({'id' : user_id})
	return dumps(user)

@app.route('/cards/<string:card_id>', methods=['GET'])
def get_user_by_card_id(card_id):
	# get user by card id
 	user = db.users.find_one({'card_id' : card_id})
	return dumps(user)

# get all users, suscribed for 'update to time' events
@app.route('/users/subscribed', methods=['GET'])
def get_subscribed_users():
	# get all subscribed users
	user = db.users.find({'dynamic_update' : '1'})
        return dumps(user) 

# register  new user
class RegisterHandler(common.EventHandler):
	def __init__(self):
		common.EventHandler.__init__(self, Event.EVENT_REGISTER_USER)

	def handle(self, data):
		db.users.update({'id' : data.get('id')}, data, upsert=True)

# class to handle 'EVENT_GO' event
class GoEventHandler(common.EventHandler):
	def __init__(self):
		common.EventHandler.__init__(self, Event.EVENT_GO)
	
	def handle(self, data):
		try:
			db.users.update({'id' : data.get('id')}, 
				{	'$set':{'dynamic_update' : '1'}}, upsert=True)	
	
		except Exception as ex:
			loggly.error(str(ex))

# class to handle 'EVENT_SLEEP' event
class SleepEventHandler(common.EventHandler):
        def __init__(self):
                common.EventHandler.__init__(self, Event.EVENT_SLEEP)

        def handle(self, data):
		try:
                	db.users.update({'id' : data.get('id')},
                                	{'$set' : {'dynamic_update' : '0'}}, upsert=True)
		except Exception as ex:
			loggly.error(str(ex)) 

if __name__ == "__main__":
	
	#defne events we are going to listen
	common.EventProcessor.event_handlers = [GoEventHandler(), SleepEventHandler(), RegisterHandler(), common.HBEventHandler(loggly)]
	# start listening for commands
        common.CmdMQConsumer(callback = common.EventProcessor.callback).start()
	# connect to user storage
	db = common.get_mongo_db(dbname='users')
    	app.run(host=common.USERS_HOST, port=common.USERS_PORT)
